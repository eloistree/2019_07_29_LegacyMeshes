# How to use: 2019_07_29_LegacyMeshes   
   
Add the following line to the [UnityRoot]/Packages/manifest.json    
``` json     
"be.eloiexperiments.legacymeshes":"https://gitlab.com/eloistree/2019_07_29_LegacyMeshes.git",    
```    
--------------------------------------    
   
Feel free to support my work: http://patreon.com/eloistree   
Contact me if you need assistance: http://eloistree.page.link/discord   
   
--------------------------------------    
``` json     
{                                                                                
  "name": "be.eloiexperiments.legacymeshes",                              
  "displayName": "Legacy Meshes",                        
  "version": "0.0.1",                         
  "unity": "2018.1",                             
  "description": "Project to project unity package design to store cool meshes that I did in the past and that I want to keep just in case it could be handy.",                         
  "keywords": ["Script","Tool","Legacy","Meshes"],                       
  "category": "Script",                   
  "dependencies":{}     
  }                                                                                
```    